package ru.kuzin.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.endpoint.IDomainEndpoint;
import ru.kuzin.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    protected IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}