package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

}