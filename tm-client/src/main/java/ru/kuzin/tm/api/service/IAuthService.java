package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.enumerated.Role;
import ru.kuzin.tm.model.User;

public interface IAuthService {

    void checkRoles(@Nullable Role[] roles);

    @Nullable
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User check(@Nullable String login, @Nullable String password);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

}