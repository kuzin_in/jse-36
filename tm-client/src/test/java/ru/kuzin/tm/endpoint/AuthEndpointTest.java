package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.endpoint.IAuthEndpoint;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.dto.request.UserLoginRequest;
import ru.kuzin.tm.dto.request.UserLogoutRequest;
import ru.kuzin.tm.dto.request.UserViewProfileRequest;
import ru.kuzin.tm.dto.response.UserLoginResponse;
import ru.kuzin.tm.dto.response.UserLogoutResponse;
import ru.kuzin.tm.dto.response.UserViewProfileResponse;
import ru.kuzin.tm.marker.SoapCategory;
import ru.kuzin.tm.service.PropertyService;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Test
    public void testLogin() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(
                new UserLoginRequest(null, null))
        );
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
    }

    @Test
    public void testLogout() {
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
        @NotNull final UserLogoutResponse userLogoutResponse = authEndpoint.logout(
                new UserLogoutRequest(userLoginResponse.getToken())
        );
        Assert.assertNotNull(userLogoutResponse);
    }

    @Test
    public void testProfile() {
        @Nullable final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
        @Nullable final UserViewProfileResponse userProfileResponse = authEndpoint.profile(
                new UserViewProfileRequest(userLoginResponse.getToken())
        );
        Assert.assertNotNull(userProfileResponse);
        Assert.assertNotNull(userProfileResponse.getUser());
    }

}