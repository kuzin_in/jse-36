package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.kuzin.tm.api.endpoint.IAuthEndpoint;
import ru.kuzin.tm.api.endpoint.IUserEndpoint;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.dto.request.*;
import ru.kuzin.tm.dto.response.UserChangePasswordResponse;
import ru.kuzin.tm.dto.response.UserLoginResponse;
import ru.kuzin.tm.dto.response.UserUpdateProfileResponse;
import ru.kuzin.tm.dto.response.UserViewProfileResponse;
import ru.kuzin.tm.marker.SoapCategory;
import ru.kuzin.tm.model.User;
import ru.kuzin.tm.service.PropertyService;

@Category(SoapCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Nullable
    private String adminToken;

    @Nullable
    private String testToken;

    @Before
    public void initTest() {
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(new UserLoginRequest("admin", "admin"));
        adminToken = adminResponse.getToken();
        @NotNull final UserLoginResponse testResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        testToken = testResponse.getToken();
    }

    @Test
    public void testChangePassword() {
        Assert.assertNotNull(testToken);
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest();
        changePasswordRequest.setToken(testToken);
        changePasswordRequest.setPassword("newPassword");
        @NotNull final UserChangePasswordResponse changePasswordResponse = userEndpoint.changeUserPassword(
                changePasswordRequest
        );
        Assert.assertNotNull(changePasswordResponse);
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test"))
        );
        changePasswordRequest.setPassword("test");
        @NotNull final UserChangePasswordResponse changePasswordBackResponse = userEndpoint.changeUserPassword(
                changePasswordRequest
        );
        Assert.assertNotNull(changePasswordBackResponse);
    }

    @Test
    public void testLock() {
        Assert.assertNotNull(testToken);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(testToken);
        Assert.assertNotNull(authEndpoint.logout(logoutRequest));

        @NotNull final UserLockRequest lockRequest = new UserLockRequest(adminToken);
        lockRequest.setLogin("test");
        Assert.assertNotNull(userEndpoint.lockUser(lockRequest));

        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test"))
        );
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken);
        unlockRequest.setLogin("test");
        userEndpoint.unlockUser(unlockRequest);
    }

    @Test
    public void testUserRegistryAndRemove() {
        @NotNull final String login = "test login";
        @NotNull final String password = "test password";
        @NotNull final String email = "email@test.tu";
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(login, password, email);
        @Nullable final User user = userEndpoint.registryUser(request).getUser();
        Assert.assertNotNull(user);
        @NotNull final UserRemoveRequest requestRemove = new UserRemoveRequest(adminToken, login);
        @Nullable final User removedUser = userEndpoint.removeUser(requestRemove).getUser();
        Assert.assertNotNull(removedUser);
    }

    @Test
    public void testUnlockUser() {
        Assert.assertNotNull(testToken);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(testToken);
        Assert.assertNotNull(authEndpoint.logout(logoutRequest));
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest("test", "test");
        @NotNull final UserLockRequest lockRequest = new UserLockRequest(adminToken);
        lockRequest.setLogin("test");
        Assert.assertNotNull(userEndpoint.lockUser(lockRequest));
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(userLoginRequest)
        );

        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken);
        unlockRequest.setLogin("test");
        Assert.assertNotNull(userEndpoint.unlockUser(unlockRequest));
        @NotNull final UserLoginResponse response = authEndpoint.login((userLoginRequest));
        Assert.assertNotNull(response);
    }

    @Test
    public void testUpdateUserProfile() {
        Assert.assertNotNull(testToken);
        @NotNull final UserViewProfileResponse userProfileResponse = authEndpoint.profile(new UserViewProfileRequest(testToken));
        Assert.assertNotNull(userProfileResponse);
        @Nullable User user = userProfileResponse.getUser();
        Assert.assertNotNull(user);

        @NotNull final UserUpdateProfileRequest updateProfileRequest = new UserUpdateProfileRequest(testToken);
        updateProfileRequest.setFirstName("firstName");
        updateProfileRequest.setLastName("lastName");
        updateProfileRequest.setMiddleName("middleName");
        @NotNull final UserUpdateProfileResponse updateProfileResponse = userEndpoint.updateUserProfile(
                updateProfileRequest
        );
        Assert.assertNotNull(updateProfileResponse);
        user = updateProfileResponse.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("lastName", user.getLastName());
        Assert.assertEquals("firstName", user.getFirstName());
        Assert.assertEquals("middleName", user.getMiddleName());
    }

}