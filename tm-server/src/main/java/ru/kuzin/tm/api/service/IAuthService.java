package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.model.Session;
import ru.kuzin.tm.model.User;

public interface IAuthService {

    @Nullable
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    String check(@Nullable String login, @Nullable String password);

    @NotNull
    Session validateToken(@Nullable String token);

    void invalidate(@Nullable Session session);

}