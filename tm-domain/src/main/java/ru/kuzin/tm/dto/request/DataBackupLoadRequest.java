package ru.kuzin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataBackupLoadRequest extends AbstractUserRequest {

    public DataBackupLoadRequest(@Nullable final String token) {
        super(token);
    }

}