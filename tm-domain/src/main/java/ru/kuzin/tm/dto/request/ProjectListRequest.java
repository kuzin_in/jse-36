package ru.kuzin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sortType;

    public ProjectListRequest(@Nullable Sort sortType) {
        this.sortType = sortType;
    }

    public ProjectListRequest(@Nullable final String token, @Nullable final Sort sortType) {
        super(token);
        this.sortType = sortType;
    }

}