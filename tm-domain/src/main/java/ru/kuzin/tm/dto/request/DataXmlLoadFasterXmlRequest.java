package ru.kuzin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataXmlLoadFasterXmlRequest extends AbstractUserRequest {

    public DataXmlLoadFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}