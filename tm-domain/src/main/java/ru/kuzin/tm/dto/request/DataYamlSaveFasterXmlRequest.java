package ru.kuzin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataYamlSaveFasterXmlRequest extends AbstractUserRequest {

    public DataYamlSaveFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}