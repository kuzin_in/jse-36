package ru.kuzin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCompleteByIndexResponse extends AbstractTaskResponse {

    public TaskCompleteByIndexResponse(@NotNull final Task task) {
        super(task);
    }

}
